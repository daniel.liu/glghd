# Intro
See if GitLab repos will behave with GitHub Desktop app

# Test and Result
* Drag and dropping html into the GitHub Desktop app works for public
  repository
* Here are some changes to show Kuma how it works
* Will need to test what happens to private repository and whether
  passwords can be used
* New documentation added

